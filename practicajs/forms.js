function mostrarForm(){
	var form = document.getElementById("form1");
	//tabla tr filas; td columnas
	var tablahtml = document.createElement('table');
	//tabla elementos
	var tr1 = document.createElement('tr');
	var tdNombre = document.createElement('td');
	var tdNombreInput = document.createElement('td');
	
	var tr2 = document.createElement('tr');
	var tdApellido = document.createElement('td');
	var tdApellidoInput = document.createElement('td');

	var tr3 = document.createElement('tr');
	var tdEmail = document.createElement('td');
	var tdEmailInput = document.createElement('td');

	var tr4 = document.createElement('tr');
	var tdTelefono = document.createElement('td');
	var tdTelefonoInput = document.createElement('td');

	var tr5 = document.createElement('tr');
	var tr6 = document.createElement('tr');
	var tdCodigo = document.createElement('td');
	var tdCodigoInput = document.createElement('td');

	//sumatorias
	var tr7 = document.createElement('tr');
	var tdSumatoria = document.createElement('td');
	var tdField1 = document.createElement('td');
	var tdField2 = document.createElement('td');

	var tr8 = document.createElement('tr');
	var tdSuma = document.createElement('td');
	var tdSumaInput = document.createElement('td');

	var tr9 = document.createElement('tr');
	var tdResta = document.createElement('td');
	var tdRestaInput = document.createElement('td');

	var tr10 = document.createElement('tr');
	var tdMultiplicacion = document.createElement('td');
	var tdMultiplicacionInput = document.createElement('td');

	//tabla valores
	var labelNombre = document.createElement("label");
	var labelApellido = document.createElement("label");

	labelNombre.innerHTML = "Nombre: ";
	labelApellido.innerHTML = "Apellidos: ";

	var inputNombre = document.createElement("input"); 
	var inputApellido = document.createElement("input"); 
	var inputNombre = document.createElement("input"); 
	var inputApellido = document.createElement("input"); 

	inputNombre.setAttribute('type',"text");
	inputNombre.setAttribute('name',"nombre");
	inputApellido.setAttribute('type',"text");
	inputApellido.setAttribute('name',"apellidos");

	var labelTelefono = document.createElement("label");
	var labelEmail = document.createElement("label");

	labelTelefono.innerHTML = "Telefono: ";
	labelEmail.innerHTML = "E-mail: ";

	var telefonoInput = document.createElement("input");
	var emailInput = document.createElement("input");
	telefonoInput.setAttribute("type", "number");
	emailInput.setAttribute("type", "email");

	var labelCodigo = document.createElement("label");
	labelCodigo.innerHTML = "Codigo: ";

	var codigoInput = document.createElement("input");
	codigoInput.setAttribute("type", "number");

	var labelSumatoria = document.createElement("label");
	labelSumatoria.innerHTML = "Sumatoria: ";

	var field1Input = document.createElement("input"); 
	var field2Input = document.createElement("input"); 

	field1Input.setAttribute("type", "number");
	field1Input.setAttribute("id", "field1");
	field2Input.setAttribute("type", "number");
	field2Input.setAttribute("id", "field2");
	field1Input.placeholder = "Dig A";
	field2Input.placeholder = "Dig B";

	var labelSuma = document.createElement("label");
    var sumaInput = document.createElement("input"); 
    labelSuma.innerHTML = "Suma: ";
	sumaInput.setAttribute("type", "number");
	sumaInput.setAttribute("id", "suma");
	sumaInput.readOnly = true;

	var labelResta = document.createElement("label");
	var restaInput = document.createElement("input"); 
	restaInput.setAttribute("type", "number");
	restaInput.setAttribute("id", "resta");
	labelResta.innerHTML = "Resta: ";
	restaInput.readOnly = true;

	var labelMultiplicacion = document.createElement("label");
	labelMultiplicacion.innerHTML = "Multiplicacion: ";
	var multiplicacionInput = document.createElement("input"); 
	multiplicacionInput.setAttribute("type", "number");
	multiplicacionInput.setAttribute("id", "multiplicacion");
	multiplicacionInput.readOnly = true;
	
	//agregando valores
	//primera fila
	tdNombre.appendChild(labelNombre);
	tr1.appendChild(tdNombre);
	tdApellido.appendChild(labelApellido);
	tr1.appendChild(tdApellido);

    //segunda fila 
	tdNombreInput.appendChild(inputNombre);
	tr2.appendChild(tdNombreInput);
	tdApellidoInput.appendChild(inputApellido);
	tr2.appendChild(tdApellidoInput);

	//tercera fila
	tdEmail.appendChild(labelEmail);
	tr3.appendChild(tdEmail);
	tdTelefono.appendChild(labelTelefono);
	tr3.appendChild(tdTelefono);

	//cuarta fila
	tdEmailInput.appendChild(emailInput);
	tr4.appendChild(tdEmailInput);
	tdTelefonoInput.appendChild(telefonoInput);
	tr4.appendChild(tdTelefonoInput);

	//quinta fila
	tdCodigo.appendChild(labelCodigo);
	tr5.appendChild(tdCodigo);
	tdCodigoInput.appendChild(codigoInput);
	tr6.appendChild(tdCodigoInput);

	//sexta fila SUMATORIAS
	tdSumatoria.appendChild(labelSumatoria);
	tr7.appendChild(tdSumatoria);
	tdField1.appendChild(field1Input);
	tr7.appendChild(tdField1);
	tdField2.appendChild(field2Input);
	tr7.appendChild(tdField2);

	//septima fila suma

	tdSuma.appendChild(labelSuma);
	tr8.appendChild(tdSuma);
	tdSumaInput.appendChild(sumaInput);
	tr8.appendChild(tdSumaInput);

	//octava fila resta
	tdResta.appendChild(labelResta);
	tr9.appendChild(tdResta);
	tdRestaInput.appendChild(restaInput);
	tr9.appendChild(tdRestaInput);

	//novena fila multiplicacion
	tdMultiplicacion.appendChild(labelMultiplicacion);
	tr10.appendChild(tdMultiplicacion);
	tdMultiplicacionInput.appendChild(multiplicacionInput);
	tr10.appendChild(tdMultiplicacionInput);

	//agregando a la tabla
	tablahtml.appendChild(tr1);
	tablahtml.appendChild(tr2);
	tablahtml.appendChild(tr3);
	tablahtml.appendChild(tr4);
	tablahtml.appendChild(tr5);
	tablahtml.appendChild(tr6);
	tablahtml.appendChild(tr7);
	tablahtml.appendChild(tr8);
	tablahtml.appendChild(tr9);
	tablahtml.appendChild(tr10);
	
	//estilos de tabla
	//tablahtml.border = '1'
	//tablahtml.cellPadding = '10'

	//agregando tabla al formulario
	form.appendChild(tablahtml);


}